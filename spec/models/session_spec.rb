# == Schema Information
#
# Table name: sessions
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  auth_token :string           not null
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_sessions_on_auth_token  (auth_token) UNIQUE
#  index_sessions_on_deleted_at  (deleted_at)
#  index_sessions_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

describe Session do
  let(:session) { FactoryBot.create(:session) }

  context 'user' do
    it 'validates its presence' do
      session.user = nil

      expect { session.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'auth_token' do
    it 'validates its presence' do
      session.auth_token = nil

      expect { session.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
