# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  commentable_type :string
#  commentable_id   :integer
#  user_id          :integer
#  text             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  deleted_at       :datetime
#
# Indexes
#
#  index_comments_on_commentable_type_and_commentable_id  (commentable_type,commentable_id)
#  index_comments_on_user_id                              (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

describe Comment do
  let(:comment) { FactoryBot.create(:answer_comment) }

  context 'commentable' do
    it 'validates its presence' do
      comment.commentable = nil

      expect { comment.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
