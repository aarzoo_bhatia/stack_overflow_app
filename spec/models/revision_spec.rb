# == Schema Information
#
# Table name: revisions
#
#  id             :integer          not null, primary key
#  revisable_type :string
#  revisable_id   :integer
#  metadata       :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_revisions_on_revisable_type_and_revisable_id  (revisable_type,revisable_id)
#

describe Revision do
  let(:revision) { FactoryBot.create(:answer_revision) }

  context 'revisable' do
    it 'validates its presence' do
      revision.revisable = nil

      expect { revision.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
