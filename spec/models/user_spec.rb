# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string
#  email           :string
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  deleted_at      :datetime
#
# Indexes
#
#  index_users_on_deleted_at  (deleted_at)
#  index_users_on_email       (email) UNIQUE
#

describe User do
  let(:user) { user = FactoryBot.create(:user) }

  context 'email' do
    it 'validates presence of email' do
      user.email = nil
      expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'validates format of email' do
      user.email = 'this should not work'
      expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'validates uniqueness of email' do
      expect { FactoryBot.create(:user, email: user.email) }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'default scope for active user' do
    it 'returns recently created active object' do
      expect{ User.find(user.id) }.not_to raise_error
    end
  end
end
