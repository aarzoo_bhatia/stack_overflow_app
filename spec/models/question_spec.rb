# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  text       :string
#  user_id    :integer
#  views      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#  wiki       :boolean          default(FALSE)
#
# Indexes
#
#  index_questions_on_deleted_at  (deleted_at)
#  index_questions_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

describe Question do
  let(:question) { question = FactoryBot.create(:question) }

  context 'text' do
    it 'validates presence of text' do
      question.text = nil
      expect { question.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'user' do
    it 'validates presence of user' do
      question.user = nil
      expect { question.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
