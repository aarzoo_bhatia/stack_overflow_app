# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  text        :string
#  question_id :integer
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  deleted_at  :datetime
#  accepted    :boolean
#
# Indexes
#
#  index_answers_on_deleted_at   (deleted_at)
#  index_answers_on_question_id  (question_id)
#  index_answers_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (question_id => questions.id)
#  fk_rails_...  (user_id => users.id)
#

describe Answer do
  let(:answer) { answer = FactoryBot.create(:answer) }

  context 'text' do
    it 'validates presence of text' do
      answer.text = nil
      expect { answer.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'user' do
    it 'validates presence of user' do
      answer.user = nil
      expect { answer.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'question' do
    it 'validates presence of question' do
      answer.question = nil
      expect { answer.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
