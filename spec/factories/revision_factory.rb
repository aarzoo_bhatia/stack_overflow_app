# == Schema Information
#
# Table name: revisions
#
#  id             :integer          not null, primary key
#  revisable_type :string
#  revisable_id   :integer
#  metadata       :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_revisions_on_revisable_type_and_revisable_id  (revisable_type,revisable_id)
#

FactoryBot.define do
  factory :question_revision, class: 'revision' do
    association :revisable, factory: :question
    metadata "{ 'text': 'This is the revised question text' }"
  end

  factory :answer_revision, class: 'revision' do
    association :revisable, factory: :answer
    metadata "{ 'text': 'This is the revised answer text' }"
  end

  factory :vote_revision, class: 'revision' do
    association :revisable, factory: :question_vote
    metadata "{ 'vote_value': 1 }"
  end
end
