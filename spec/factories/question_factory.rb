# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  text       :string
#  user_id    :integer
#  views      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#  wiki       :boolean          default(FALSE)
#
# Indexes
#
#  index_questions_on_deleted_at  (deleted_at)
#  index_questions_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :question do
    association :user
    text "what is this?"
    views 0

    before(:create) do |question, evaluator|
      question.user_id = evaluator[:user].try(:id) || User.first || FactoryBot.create(:user).id
    end

    after(:create) do |question, evaluator|
      tag_ids = evaluator[:tags].try(:ids)
      if not tag_ids.present?
        tag = Tag.take || FactoryBot.create(:tag)
        tag_ids = [tag.id]
      end
      question.tag_ids = tag_ids
      question.save!
    end
  end
end
