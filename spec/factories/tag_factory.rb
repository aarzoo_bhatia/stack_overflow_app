# == Schema Information
#
# Table name: tags
#
#  id          :integer          not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  name        :text
#  deleted_at  :datetime
#

FactoryBot.define do
  factory :tag do |f|
    f.name { Faker::Name.name }
    f.description "this is it."
  end
end
