# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  commentable_type :string
#  commentable_id   :integer
#  user_id          :integer
#  text             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  deleted_at       :datetime
#
# Indexes
#
#  index_comments_on_commentable_type_and_commentable_id  (commentable_type,commentable_id)
#  index_comments_on_user_id                              (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :question_comment, class: 'comment' do
    association :commentable, factory: :question
    association :user
    text "This is a comment."
  end

  factory :answer_comment, class: 'comment' do
    association :commentable, factory: :answer
    association :user
    text "thats a comment"
  end
end
