# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  text       :string
#  user_id    :integer
#  views      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#  wiki       :boolean          default(FALSE)
#
# Indexes
#
#  index_questions_on_deleted_at  (deleted_at)
#  index_questions_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class Question < ApplicationRecord
  revisable
  commentable
  votable
  allow_soft_delete

  validates_presence_of :text, :user

  belongs_to :user

  has_many :answers, -> { where(deleted_at: nil) }
  has_and_belongs_to_many :tags

  after_update :create_revision
end
