# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  text        :string
#  question_id :integer
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  deleted_at  :datetime
#  accepted    :boolean
#
# Indexes
#
#  index_answers_on_deleted_at   (deleted_at)
#  index_answers_on_question_id  (question_id)
#  index_answers_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (question_id => questions.id)
#  fk_rails_...  (user_id => users.id)
#

class Answer < ApplicationRecord
  revisable
  commentable
  votable
  allow_soft_delete

  validates_presence_of :text, :user, :question

  belongs_to :user
  belongs_to :question

  after_save :create_revision
end
