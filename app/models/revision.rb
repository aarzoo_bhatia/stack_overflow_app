# == Schema Information
#
# Table name: revisions
#
#  id             :integer          not null, primary key
#  revisable_type :string
#  revisable_id   :integer
#  metadata       :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_revisions_on_revisable_type_and_revisable_id  (revisable_type,revisable_id)
#

class Revision < ApplicationRecord
  validates_presence_of :revisable

  belongs_to :revisable, polymorphic: true
end
