# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string
#  email           :string
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  deleted_at      :datetime
#
# Indexes
#
#  index_users_on_deleted_at  (deleted_at)
#  index_users_on_email       (email) UNIQUE
#

class User < ApplicationRecord
  allow_soft_delete
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  has_secure_password

  validates_presence_of :name, :email, :password

  validates :name, length: { maximum: 100 }
  validates :email, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }

  has_many :questions
  has_many :answers
  has_many :comments
  has_many :votes
  has_many :sessions, dependent: :destroy

  before_save :prepare_email

  def prepare_email
    self.email.downcase!
  end

end
