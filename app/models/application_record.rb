class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def create_revision
    Revision.create(revisable: self, metadata: to_json(except: [id, created_at, updated_at]))
  end

  class <<self
    def commentable
      has_many :comments, -> { where(deleted_at: nil) }, as: :commentable
    end

    def revisable
      has_many :revisions, as: :revisable
    end

    def votable
      has_many :votes, as: :votable
    end

    def allow_soft_delete
      default_scope { where(deleted_at: nil) }

      alias_method :hard_destroy, :destroy

      def destroy
        self.update(deleted_at: Time.now)
      end
    end
  end

end
