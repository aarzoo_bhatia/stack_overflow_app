# == Schema Information
#
# Table name: tags
#
#  id          :integer          not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  name        :text
#  deleted_at  :datetime
#

class Tag < ApplicationRecord
  allow_soft_delete
  validates :name, presence: true, length: { maximum: 100, too_long: "%{count} is the maximum characters allowed" }

  has_and_belongs_to_many :questions
end
