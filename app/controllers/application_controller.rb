require_relative '../services/authentication_service'
require_relative '../services/error_handling_service'

class ApplicationController < ActionController::Base
  include AuthenticationService
  include ErrorHandlingService

  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token
  before_action :authorize, except: [:login, :show, :index]
end

