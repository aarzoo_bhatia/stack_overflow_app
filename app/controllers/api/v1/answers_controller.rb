require_relative '../crud_controller'

module Api::V1
# Controller for performing operations on Answer model
class AnswersController < CRUDController
  private

  # Returns answer details for -
  # 1. Given question_id {params: question_id} or
  # 2. All answers
  def read_model
    return Question.find(params[:question_id]).answers if params[:question_id]
    Answer.all
  end

  # Model used for CRUD operations
  def model
    current_user.answers
  end

  # Strong parameters
  def filtered_params
    params.require(:answer).permit(:text)
  end

  def index
    render json: read_model, status: :ok
  end

  def show
    render json: read_model.find(params[:id]), status: :ok
  end

  # Creating answer for a particular question
  def create
    render json: model.where(question_id: params[:question_id]).create!(filtered_params), status: :created
  end
end
end