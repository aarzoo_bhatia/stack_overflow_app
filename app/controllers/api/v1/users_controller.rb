require_relative '../crud_controller'

module Api::V1
  # Controller for handling actions on User model
  class UsersController < CRUDController
  skip_before_action :authorize, only: :create

  # Used for obtaining strong parameters
  def filtered_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  # Used for updating current user's attributes
  def update
    render json: current_user.update_attributes!(filtered_params), status: :ok
  end

  # Destroy the current user by setting the value of deleted_at
  def destroy
    render json: current_user.destroy!, status: :ok
  end

  # Model name for working with CRUD operations
  def model
    User
  end
  end
end