module Api::V1
# Controller for hadnling operations on Vote model
class VotesController < ApplicationController
  # Creating vote for a user
  # - only if no existing vote is found for that entity
  def create
    votable = question_or_answer
    @vote = Vote.find_by(votable: votable, user_id: cookies.signed[:user_id])

    if @vote
      return render json: { error: 'Vote has already been given' }, status: 200 if @vote[:vote_value] == vote_params[:vote_value]
      @vote[:vote_value] = vote_params[:vote_value]
    else
      @vote = Vote.new(votable: votable, user_id: cookies.signed[:user_id], vote_value: vote_params[:vote_value])
    end

    return render json: @vote.errors, status: 400 unless @vote.save
    render json: @vote, status: 201
  end

  # Function for obtaining the applicable votable entity.
  def question_or_answer
    create_params = vote_params
    Answer.find_by(id: create_params[:answer_id]) if create_params[:answer_id]
    Question.find_by(id: create_params[:question_id]) if create_params[:question_id]
  end

  # Vote can belong to a question or answer
  # Function returns the question_id or answer_id accordingly
  def vote_params
    return_params = params.require(:vote).permit(:vote_value, :question_id, :answer_id)
    return render json: { error: "Invalid parameters" }, status: 400 if (params[:question_id] && params[:answer_id]) || (!params[:question_id] && !params[:answer_id])
    return_params[:question_id] = params[:question_id]
    return_params[:answer_id] = params[:answer_id]
    return_params
  end
end
end