require_relative '../crud_controller'

module Api::V1
# Controller for performing operations on Question model
class QuestionsController < CRUDController

  # Function for fetching question details for -
  # 1. Particular id {params : id} or
  # 2. Particular User {params : user} or
  # 3. All questions
  def read_model
    return Question.where(user_id: params[:user_id]) if params[:user_id]
    return Question.includes(:comments, {:answers => :comments}) if params[:id]
    Question.all
  end

  # Model definition for CRUD operations
  def model
    current_user.questions
  end

  # Overridden index method
  def index
    render json: read_model, status: :ok
  end

  # Overridden show method
  def show
    render json: read_model.find(params[:id]), status: :ok
  end

  # function for returning strong params
  def filtered_params
    params.require(:question).permit(:text)
  end
end
end



