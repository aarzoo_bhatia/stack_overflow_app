require_relative '../crud_controller'

module Api::V1
# Controller for performing operations on Comment model
class CommentsController < CRUDController
  # Creating comments for a question or answer
  def create
    render json: Comment.create!(comment_params.merge(commentable: question_or_answer, user_id: cookies.signed[:user_id])), status: 201
  end

  private

  # Returns question if params has answer_id
  # Returns answer if params has question_id
  def question_or_answer
    return Answer.find(params[:answer_id]) if params[:answer_id]
    return Question.find(params[:question_id]) if params[:question_id]
  end

  # Model for performing CRUD operations
  def model
    current_user.comments
  end

  # Strong parameters enforced
  def comment_params
    params.require(:comment).permit(:text)
  end
end
end