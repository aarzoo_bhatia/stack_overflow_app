# General Controller for CRUD operations on all models
class CRUDController < ApplicationController
  def index
    render json: model, status: :ok
  end

  def show
    render json: model.find(params[:id]), status: :ok
  end

  def create
    render json: model.create!(filtered_params), status: :created
  end

  def update
    render json: model.find(params[:id]).update_attributes!(filtered_params), status: :ok
  end

  def destroy
    render json: model.find(params[id]).destroy!, status: :ok
  end
end