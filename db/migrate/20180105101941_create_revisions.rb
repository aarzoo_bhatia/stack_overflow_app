class CreateRevisions < ActiveRecord::Migration[5.1]
  def change
    create_table :revisions do |t|
        t.references :revisable, polymorphic: true, index: true
        t.jsonb :metadata
        t.timestamps

    end
  end
end
