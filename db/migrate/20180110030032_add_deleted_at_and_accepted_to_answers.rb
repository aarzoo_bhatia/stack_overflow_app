class AddDeletedAtAndAcceptedToAnswers < ActiveRecord::Migration[5.1]
  def change
    add_column :answers, :deleted_at, :datetime
    add_column :answers, :accepted, :boolean
    add_index :answers, :deleted_at
  end
end
