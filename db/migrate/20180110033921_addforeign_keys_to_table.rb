class AddforeignKeysToTable < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :answers, :users, column: :user_id
    add_foreign_key :answers, :questions, column: :question_id
    add_foreign_key :questions, :users, column: :user_id
    add_foreign_key :comments, :users, column: :user_id
  end
end
