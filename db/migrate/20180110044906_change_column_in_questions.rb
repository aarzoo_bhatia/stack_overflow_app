class ChangeColumnInQuestions < ActiveRecord::Migration[5.1]
  def up
    change_column :questions, :wiki, :boolean, default: false
  end

  def down
    change_column :questions, :wiki, :boolean, default: nil
  end

end
