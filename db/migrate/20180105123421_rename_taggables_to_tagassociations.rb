class RenameTaggablesToTagassociations < ActiveRecord::Migration[5.1]
  def change
    def self.up
      rename_table :taggables, :tagassociations
    end

    def self.down
      rename_table :tagassociations, :taggables
    end
  end
end
