# Seeding for votes and comments
module Votesandcommentsseeds
  def self.insert
answers = Answer.first(3)
votes = []
comments = []
users = User.all
questions = Question.all
questions.each do |question|
  users.each do |user|
    votes << { vote_value:  1, votable: question, user: user }
    comments << { text:  "Comment for question #{question[:id]}", commentable: question, user: user }
  end
end

answers.each do |answer|
  users.each do |user|
    votes << { vote_value: -1, votable: answer, user: user }
    comments << { text: "Comment for answer #{answer[:id]}", commentable: answer, user: user }
  end
end
Vote.create(votes)
Comment.create(comments)
end
end