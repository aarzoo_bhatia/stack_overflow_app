# Seeding for answers table
module Answerseeds
  def self.insert
    questions = Question.first(3)
    answers = []
    users = User.all
    questions.each do |question|
      users.each { |user| answers << { text: "This is answer for question #{question.id} by user #{user.id}", question: question, user: user } }
    end
    Answer.create(answers)
  end
end
