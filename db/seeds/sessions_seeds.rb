# Seeding for sessions
module Sessionseeds
  def self.insert
users = User.all
sessions = []
users.each do |user|
  3.times { sessions << { user: user, auth_token: SecureRandom.hex(12) } }
end
Session.create(sessions)
end
end