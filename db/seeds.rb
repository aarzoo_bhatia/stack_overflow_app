# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require_relative './seeds/users_seeds.rb'
require_relative './seeds/questions_seeds.rb'
require_relative './seeds/sessions_seeds.rb'
require_relative './seeds/answers_seeds.rb'
require_relative './seeds/votes_and_comments_seeds.rb'
require_relative './seeds/tags_seeds.rb'
require_relative './seeds/revisions_seeds.rb'
require_relative './seeds/questions_tags_seeds.rb'

p 123
Userseeds.insert
Questionseeds.insert
Sessionseeds.insert
Answerseeds.insert
Votesandcommentsseeds.insert
Tagseeds.insert
Revisionseeds.insert
Questiontagseeds.insert
