require 'api_constraints'

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api, path: '/' do
    scope module: :v1,
          constraints: ApiConstraints.new(version: 1, default: true) do
      concern :commentable do
        resources :comments, shallow: true
      end

      concern :votable do
        resources :votes, shallow: true
      end

      resources :users do
        resources :questions, shallow: true, concerns: [:commentable, :votable] do
          resources :answers, shallow: true, concerns: [:commentable, :votable]
        end
      end

      get '/questions', to: 'questions#index'
      post '/login', to: 'sessions#login'
      delete '/logout', to: 'sessions#destroy'
    end
  end
end

